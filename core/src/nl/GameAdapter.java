package nl;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import nl.states.GameStateManager;

/**
 * Entry point for game, delegates input, rendering and updates logic
 */
public class GameAdapter extends ApplicationAdapter {
    public static Application.ApplicationType applicationType;

    private GameStateManager gameStateManager;

    public static int versionCode;
    public static String versionName;

    public GameAdapter(int versionCode, String versionName) {
        GameAdapter.versionCode = versionCode;
        GameAdapter.versionName = versionName;
    }

    @Override
    public void create() {
        applicationType = Gdx.app.getType();
        gameStateManager = new GameStateManager();
        if (applicationType != Application.ApplicationType.Android) {
            Preferences prefs = Gdx.app.getPreferences("Settings");
            int width = prefs.getInteger("width", 1024);
            int height = prefs.getInteger("height", 786);
            if (!(Gdx.graphics.getWidth() == width && Gdx.graphics.getHeight() == height)) {
                Gdx.graphics.setWindowedMode(width, height);
            }
        } else {
            Gdx.input.setCatchBackKey(true);
        }
    }

    @Override
    public void render() {
        float deltaTime = Gdx.graphics.getDeltaTime();

        if (gameStateManager != null) {
            gameStateManager.update(deltaTime);
            gameStateManager.draw();
        }

        super.render();
    }

    @Override
    public void pause() {
        if (gameStateManager != null) {
            gameStateManager.pause();
        }
        super.pause();
    }

    @Override
    public void resume() {
        if (gameStateManager != null) {
            gameStateManager.resume();
        }
        super.resume();
    }

    @Override
    public void dispose() {
        if (gameStateManager != null) {
            gameStateManager.dispose();
        }
        super.dispose();
    }

    @Override
    public void resize(int width, int height) {
        if (gameStateManager != null) {
            gameStateManager.resize(width, height);
        }
        super.resize(width, height);
    }
}
