package nl.logic;
import java.util.ArrayList;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;

public class Simulation {
    private static ArrayList<ChemicalBond> chemicalBonds;
    static ArrayList<Molecule> molecules;
    private static ArrayList<Molecule> reactedMolecules;
    private final ArrayList<Reaction> reactions = new ArrayList<>();
    private final ArrayList<Atom> allAtoms;
    private final ArrayList<Rectangle> walls;
    private static boolean reset = false;
    public static ReactionType reactiontype = ReactionType.HYDROGEN;

    private final float panning = 1f;
    private final float playwidth, playheight;

    public Simulation() {
        System.out.println("START SIMULATION\n");
        float width = 10f;
        float height = 5.625f; // 16:9 ratio

        playwidth = width - 2*panning;
        playheight = height - 2*panning;
        allAtoms = new ArrayList<>();
        walls = new ArrayList<>();

        chemicalBonds = new ArrayList<>();
        molecules = new ArrayList<>();
        reactedMolecules = new ArrayList<>();

        float wallWidth = 1f;
        walls.add(new Rectangle(-wallWidth, -wallWidth, wallWidth, height + 2 * wallWidth)); // Left wall
        walls.add(new Rectangle(width, -wallWidth, wallWidth, height + 2 * wallWidth)); // Right wall
        walls.add(new Rectangle(0, -wallWidth, width, wallWidth)); // Bottom wall
        walls.add(new Rectangle(0, height, width, wallWidth)); // Top wall

        switch (reactiontype) {
            case HYDROGEN:
                SetupMolecules("hh","oo",42,21, 3, 3);
                break;
            case METHANE:
                SetupMolecules("hhchh", "oo", 21, 42, 4, 3);
                break;
            case AMMONIA:
                SetupMolecules("hnhh","oo", 16, 20, 3,3);
                break;
            case NITROGEN:
                SetupMolecules("nn","oo",21,21,3,3);
        }

        String[] reactings1 = {"hh", "hh", "hhchh","hhchh","co", "co", "o", "hnhh","hnhh","hnhh","h", "h", "h",  "h", "hh", "oh", "nn","n", "n", "n", "nn"};
        String[] reactings2 = {"oo", "o",  "oo",   "o",    "o",  "oo", "o", "oo",  "o",   "oh",  "oo","o", "oh", "h", "oh", "oh", "oo","n", "o", "oo","o"};
        String[] products1 =  {"hoh","hoh","hoh",  "hh",   "oco","oco","oo","hoh", "hh",  "hoh", "oh","oh","hoh","hh","hoh","hoh","no","nn","no","no","no"};
        String[] products2 =  {"o",  "",   "hh",   "hh",   "",   "o",  "",  "no",  "no",  "n",   "o", "",  "",   "",  "",   "o",  "no","",  "",  "o", "n"};
        String[] products3 =  {"",   "",   "co",   "co",   "",   "",   "",  "h",   "h",   "hh",  "",  "",  "",   "",  "h",  "",   "",  "",  "",  "",  "" };

        for (int i = 0; i < reactings1.length; i++) {
            reactions.add(new Reaction(reactings1[i], reactings2[i], products1[i], products2[i], products3[i]));
        }

        System.out.println("Number of start molecules = " + molecules.size());
        updateAllAtoms();
    }

    private void SetupMolecules(String firstIn, String secondIn, int amount1, int amount2, int numberRows1, int numberRows2) {
        float[] places1 = new float[numberRows1];
        float[] places2 = new float[numberRows1];

        for (int row1 = 0; row1 < numberRows1; row1++) {
            places1[row1] = panning + row1 * playwidth / (numberRows1 + numberRows2 - 1);
        }
        for (int row2 = 0; row2 < numberRows2; row2++) {
            places2[row2] = playwidth + panning - row2 * playwidth / (numberRows1 + numberRows2 - 1);
        }

        addMolecules(firstIn, amount1, numberRows1, places1);
        addMolecules(secondIn, amount2, numberRows2, places2);
    }

    private void addMolecules(String elements, int amountMolecules, int numberRows, float[] places){
        for (int i = 0; i < amountMolecules; i++) {
            Molecule molecule = elements.equals("hhchh") ? new Methane(elements) : elements.equals("hnhh") ? new Ammonia(elements) : new Molecule(elements);
            float x = 0;
            for (int row = 0; row < numberRows; row++) {
                if (i % numberRows == row) {
                    x = places[row];
                }
            }
            // molecule.ArrangeAtoms(panning + playwidth * MathUtils.random(), panning + playheight * MathUtils.random());
            molecule.ArrangeAtoms(x, panning + i * playheight / (float) amountMolecules);
            molecule.ArrangeChemicalBonds();
            molecules.add(molecule);
        }
    }

    private void updateAllAtoms() {
        allAtoms.clear();
        chemicalBonds.clear();
        for (Molecule molecule : molecules) {
            allAtoms.addAll(molecule.atoms);
            chemicalBonds.addAll(molecule.chemicalBonds);
        }
        System.out.println("Number of atoms = " + allAtoms.size());
    }

    public void update(float timeDelta) {
        if (Simulation.reset) {
            reset();
            Simulation.reset = false;
        } else {
            while (!reactedMolecules.isEmpty()) {
                System.out.println("Reaction starts; " + reactedMolecules.size() + " molecules reacted");
                Molecule firstMolecule = reactedMolecules.get(0);
                Molecule secondMolecule = reactedMolecules.get(1);

                molecules.remove(firstMolecule);
                molecules.remove(secondMolecule);
                reactedMolecules.remove(0);
                reactedMolecules.remove(0);

                System.out.println("firstMolecule.atoms.size() = " + firstMolecule.atoms.size());
                System.out.println("secondMolecule.atoms.size() = " + secondMolecule.atoms.size());

                Reaction reaction = getCurrentReaction(firstMolecule, secondMolecule);
                if (reaction != null) {
                    reaction.makeRemainingAtoms(firstMolecule, secondMolecule);
                    molecules.add(new Molecule(reaction.getRearrangedAtomsProduct(reaction.product1)));

                    if (!reaction.product2.isEmpty()) {
                        molecules.add(new Molecule(reaction.getRearrangedAtomsProduct(reaction.product2)));
                    }
                    if (!reaction.product3.isEmpty()) {
                        molecules.add(new Molecule(reaction.getRearrangedAtomsProduct(reaction.product3)));
                    }
                } else {
                    System.out.println("reaction not found exception");
                }

                System.out.println("Number of molecules = " + molecules.size());

                updateAllAtoms();
                System.out.println("Reaction ends\n");
            }


            // Update the position of the moving molecule
            for (Molecule molecule : molecules) {
                if (molecule.reactable) {
                    molecule.calcAverageAtomPosition();
                }
                molecule.updateCovalentForces(timeDelta);
                molecule.updateAtoms(timeDelta, walls, allAtoms);
            }

            for (Molecule molecule : molecules) {
                if (molecule.reactable) {
                    if (molecule.isReacted()) {
                        if (!reactedMolecules.contains(molecule) && !reactedMolecules.contains(molecule.reacted)) {
                            reactedMolecules.add(molecule.reacted);
                            System.out.println(molecule.reacted.elements + " molecule added to reactedMolecules at index: " + (reactedMolecules.size() - 1));
                            reactedMolecules.add(molecule);
                            System.out.println(molecule.elements + " molecule added to reactedMolecules at index: " + (reactedMolecules.size() - 1));
                        }
                        molecule.reacted = null;
                    }
                }
            }
        }
    }

    private Reaction getCurrentReaction(Molecule first, Molecule second) {
        for (Reaction reaction : reactions) {
            if (first.elements.equals(reaction.reacting1) && second.elements.equals(reaction.reacting2)) {
                return reaction;
            }
        }
        return null;
    }

    private void reset() {
        for (Atom atom : allAtoms) {
            atom.initializeVectors();
        }

        for (ChemicalBond chemicalBond : getChemicalBonds()) {
            chemicalBond.initialize();
        }
    }

    public void moveAtomsRight() {
        for (Molecule molecule : molecules) {
            if (!molecule.reactable) {
                for (Atom atom : molecule.atoms) {
                    atom.setXVelocity(0.02f);
                }
            }
        }
    }

    public void moveAtomsLeft() {
        for (Molecule molecule : molecules) {
            if (molecule.reactable) {
                for (Atom atom : molecule.atoms) {
                    atom.setXVelocity(-0.02f);
                }
            }
        }
    }

    public void moveAtomsUp() {
        for (Molecule molecule : molecules) {
            if (!molecule.reactable) {
                for (Atom atom : molecule.atoms) {
                    atom.setYVelocity(0.02f);
                }
            }
        }
    }

    public void moveAtomsDown() {
        for (Molecule molecule : molecules) {
            if (molecule.reactable) {
                for (Atom atom : molecule.atoms) {
                    atom.setYVelocity(-0.02f);
                }
            }
        }
    }

    public ArrayList<ChemicalBond> getChemicalBonds() {
        return chemicalBonds;
    }

    public ArrayList<Atom> getAllAtoms() {
        return allAtoms;
    }

    public ArrayList<Rectangle> getWalls() {
        return walls;
    }

    public void pause() {

    }

    public void resume() {

    }
}
