package nl.logic;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import java.util.ArrayList;

class Molecule {
    private float averageX = 0, averageY = 0;
    float covalentLength = 0.25f;
    private float startDirection;
    private final int numberAtoms;
    public boolean reactable;
    ArrayList<Atom> atoms = new ArrayList<>();
    ArrayList<ChemicalBond> chemicalBonds = new ArrayList<>();
    ArrayList<Vector2> covalentForces = new ArrayList<>();
    String elements;
    Molecule reacted;

    Molecule(String elements) {
        this.elements = elements;
        this.numberAtoms = elements.length();
        this.reactable = true;
        this.startDirection = 2 * MathUtils.PI * MathUtils.random();
    }

    Molecule(ArrayList<Atom> atomsArg) {
        this.atoms = atomsArg;
        this.numberAtoms = atomsArg.size();
        elements = "";
        for (Atom atom : atoms) {
            elements = elements + atom.element;
        }

        this.reactable = !elements.equals("hoh") && !elements.equals("oco") && !elements.equals("no");
        System.out.println("new molecule has elements: " + elements);
        chemicalBonds.clear();
        covalentForces.clear();

        for (Atom atom : atoms) {
            atom.attached = atoms.size() > 1;
            atom.collided = false;
            atom.bounceOffAtom = null;
        }

        ArrangeChemicalBonds();
    }

    public void ArrangeAtoms(float startX, float startY) {
        for (int i = 0; i < numberAtoms; i++) {
            startX += covalentLength * i;
            Atom atom = new Atom(elements.charAt(i), new Vector2(startX, startY), numberAtoms > 1);

            atom.setVelocity(getStartVelocity());
            atoms.add(atom);
        }
    }

    protected Vector2 getStartVelocity() {
        return new Vector2(0.25f * MathUtils.cos(startDirection), 0.25f * MathUtils.sin(startDirection));
    }

    public void ArrangeChemicalBonds(){
        for (int i = 0; i < numberAtoms - 1; i++) {
            chemicalBonds.add(new ChemicalBond(covalentLength, true));
            covalentForces.add(new Vector2(0,0));
        }

        if (elements.equals("hoh")){
            chemicalBonds.add(new ChemicalBond(MathUtils.sin(0.9119f) * covalentLength * 2f, false));
            covalentForces.add(new Vector2(0,0));
        }
        if (elements.equals("oco")){
            chemicalBonds.add(new ChemicalBond(covalentLength * 2f, false));
            covalentForces.add(new Vector2(0,0));
        }
    }

    void updateAtoms(float timeDelta, ArrayList<Rectangle> walls, ArrayList<Atom> allAtoms) {
        for (int i = 0; i < atoms.size(); i++) {
            Atom atom = atoms.get(i);
            if (atom.attached) {
                if (elements.equals("hoh") || elements.equals("oco")){
                    if (i == 0) {
                        atom.processMovement(timeDelta, walls, allAtoms, covalentForces.get(2).rotate(180), covalentForces.get(i).rotate(180));
                    } else {
                        atom.processMovement(timeDelta, walls, allAtoms, covalentForces.get(i - 1).rotate(180), covalentForces.get(i).rotate(180));
                    }
                } else {
                    if (i == 0) { // first atom
                        atom.processMovement(timeDelta, walls, allAtoms, covalentForces.get(0).rotate(180));
                    } else if (i == atoms.size() - 1) { // last atom
                        atom.processMovement(timeDelta, walls, allAtoms, covalentForces.get(atoms.size() - 2).rotate(180));
                    } else { // only for elements.size > 2
                        atom.processMovement(timeDelta, walls, allAtoms, covalentForces.get(i - 1).rotate(180), covalentForces.get(i).rotate(180));
                    }
                }
            } else {
                atom.processMovement(timeDelta, walls, allAtoms);
            }
        }
    }

    public void calcAverageAtomPosition(){
        float x = 0;
        float y = 0;
        for (Atom atom : atoms){
            x += atom.x;
            y += atom.y;
        }
        averageX = x / atoms.size();
        averageY = y / atoms.size();
    }

    boolean isReacted() {
        //calcAverageAtomPosition();

        for (Molecule molecule : Simulation.molecules) {
            if (!molecule.reactable){
                continue;
            }
            if (molecule != this) {
                if (Math.abs(averageX - molecule.averageX) > 0.5f || Math.abs(averageY - molecule.averageY) > 0.5f) {
                    continue;
                }
            } else {
                continue;
            }

            String reacting = molecule.elements;
            switch (elements) {
                case "oo":
                    if (reacting.equals("hh") || reacting.equals("hhchh") || reacting.equals("hnhh") || reacting.equals("h") || reacting.equals("co") || reacting.equals("nn")) {
                        reacted = molecule;
                        return true;
                    } else {
                        return false;
                    }
                case "o":
                    if (reacting.equals("hh") || reacting.equals("hhchh") || reacting.equals("hnhh") || reacting.equals("h") || reacting.equals("co") || reacting.equals("n") || reacting.equals("o")) {
                        reacted = molecule;
                        return true;
                    } else {
                        return false;
                    }
                case "oh":
                    if (reacting.equals("hh") || reacting.equals("hhchh") || reacting.equals("hnhh") || reacting.equals("h") || reacting.equals("oh")) {
                        reacted = molecule;
                        return true;
                    } else {
                        return false;
                    }
                case "n":
                    if (reacting.equals("n")){
                        reacted = molecule;
                        return true;
                    } else {
                        return false;
                    }
                case "h":
                    if (reacting.equals("h")){
                        reacted = molecule;
                        return true;
                    } else {
                        return false;
                    }
            }
        }
        return false;
    }

    void updateCovalentForces(float timeDelta) {
        for (int i = 0; i < chemicalBonds.size(); i++) {
            ChemicalBond chemicalBond = chemicalBonds.get(i);
            if ((elements.equals("hoh") || elements.equals("oco")) && i == 2) {
                covalentForces.get(i).set(chemicalBond.updateForce(atoms.get(0).getPosition(), atoms.get(2).getPosition(), timeDelta));
            } else {
                covalentForces.get(i).set(chemicalBond.updateForce(atoms.get(i).getPosition(), atoms.get(i + 1).getPosition(), timeDelta));
            }

            // Update the chemical bonds
            chemicalBond.updatePoints();
        }
    }

}