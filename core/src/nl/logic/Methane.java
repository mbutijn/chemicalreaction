package nl.logic;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import java.util.ArrayList;

public class Methane extends Molecule {
    Methane(String elements) {
        super(elements);
    }

    @Override
    public void ArrangeAtoms(float startX, float startY){
        float x = 0, y = 0;

        for (int i = 0; i < 5; i++) {
            if (i == 0) {
                x = startX - covalentLength;
                y = startY;
            } else if (i == 1) {
                x = startX;
                y = startY + covalentLength;
            } else if (i == 2) {
                x = startX;
                y = startY;
            } else if (i == 3) {
                x = startX;
                y = startY - covalentLength;
            } else if (i == 4) {
                x = startX + covalentLength;
                y = startY;
            }

            Atom atom = new Atom(elements.charAt(i), new Vector2(x, y), true);
            atom.setVelocity(getStartVelocity());
            atoms.add(atom);
        }
    }

    @Override
    public void ArrangeChemicalBonds() {
        super.ArrangeChemicalBonds();

        for (int i = 0; i < 4; i++) {
            chemicalBonds.add(new ChemicalBond((float) Math.sqrt(2) * covalentLength,  false));
            covalentForces.add(new Vector2(0, 0));
        }
        chemicalBonds.add(new ChemicalBond(2f * covalentLength, false));
        covalentForces.add(new Vector2(0, 0));
        chemicalBonds.add(new ChemicalBond(2f * covalentLength,  false));
        covalentForces.add(new Vector2(0, 0));
    }

    @Override
    void updateCovalentForces(float timeDelta) {
        for (int i = 0; i < chemicalBonds.size(); i++) {
            ChemicalBond chemicalBond = chemicalBonds.get(i);
            Vector2 force;
            if (i == 0 || i == 1) { // on carbon and hydrogen (visible)
                force = chemicalBond.updateForce(atoms.get(i).getPosition(), atoms.get(2).getPosition(), timeDelta);
            } else if (i == 2 || i == 3) {
                force = chemicalBond.updateForce(atoms.get(2).getPosition(), atoms.get(i + 1).getPosition(), timeDelta);
            } else if (i == 4) { // on 2 hydrogen atoms (invisible)
                force = chemicalBond.updateForce(atoms.get(0).getPosition(), atoms.get(1).getPosition(), timeDelta);
            } else if (i == 5) {
                force = chemicalBond.updateForce(atoms.get(0).getPosition(), atoms.get(3).getPosition(), timeDelta);
            } else if (i == 6) {
                force = chemicalBond.updateForce(atoms.get(1).getPosition(), atoms.get(4).getPosition(), timeDelta);
            } else if (i == 7) {
                force = chemicalBond.updateForce(atoms.get(3).getPosition(), atoms.get(4).getPosition(), timeDelta);
            } else if (i == 8) {
                force = chemicalBond.updateForce(atoms.get(0).getPosition(), atoms.get(4).getPosition(), timeDelta);
            } else { // i == 9
                force = chemicalBond.updateForce(atoms.get(1).getPosition(), atoms.get(3).getPosition(), timeDelta);
            }

            covalentForces.get(i).set(force);

            // Update the chemical bonds
            chemicalBond.updatePoints();
        }
    }

    @Override
    void updateAtoms(float timeDelta, ArrayList<Rectangle> walls, ArrayList<Atom> allAtoms) {
        for (int i = 0; i < atoms.size(); i++) {
            Atom atom = atoms.get(i);
            if (i == 0){
                atom.processMovement(timeDelta, walls, allAtoms, covalentForces.get(i).rotate(180), covalentForces.get(4).rotate(180), covalentForces.get(5).rotate(180), covalentForces.get(8).rotate(180));
            } else if (i == 1) {
                atom.processMovement(timeDelta, walls, allAtoms, covalentForces.get(i).rotate(180), covalentForces.get(4).rotate(180), covalentForces.get(6).rotate(180), covalentForces.get(9).rotate(180));
            } else if (i == 2) {
                atom.processMovement(timeDelta, walls, allAtoms, covalentForces.get(0).rotate(180), covalentForces.get(1).rotate(180), covalentForces.get(2).rotate(180), covalentForces.get(3).rotate(180));
            } else if (i == 3) {
                atom.processMovement(timeDelta, walls, allAtoms, covalentForces.get(i - 1).rotate(180), covalentForces.get(5).rotate(180), covalentForces.get(7).rotate(180), covalentForces.get(9).rotate(180));
            } else if (i == 4) {
                atom.processMovement(timeDelta, walls, allAtoms, covalentForces.get(i - 1).rotate(180), covalentForces.get(6).rotate(180), covalentForces.get(7).rotate(180), covalentForces.get(8).rotate(180));
            }
        }
    }

}
