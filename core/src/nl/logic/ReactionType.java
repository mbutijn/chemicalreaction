package nl.logic;

public enum ReactionType {
    HYDROGEN,
    METHANE,
    AMMONIA,
    NITROGEN
}
