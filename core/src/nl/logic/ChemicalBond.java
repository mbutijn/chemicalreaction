package nl.logic;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

public class ChemicalBond {
    private final float[][] springPercentages = {{0, 0}, {0, 0.15f}, {1f, 0.2f}, {-1f, 0.3f}, {1f, 0.4f}, {-1f, 0.5f}, {1f, 0.6f}, {-1f, 0.7f}, {1f, 0.8f}, {0, 0.85f}, {0, 1}};
    private final float equilibriumDistance, width, stiffness, damping; // these properties do not change throughout simulation
    private float currentLength, oldLength; // these properties change throughout simulation
    private final Vector2 attachmentMass, initialAttachment, vector, force;
    private final Vector2[] points, rotatedPoints;
    public boolean draw;

    ChemicalBond(float equilibriumDistance, boolean draw) {
        this.equilibriumDistance = equilibriumDistance;
        this.width = draw ? 0.03f : 0;
        this.stiffness = -100;
        this.damping = -1;
        this.points = new Vector2[springPercentages.length];
        this.rotatedPoints = new Vector2[springPercentages.length];
        this.initialAttachment = new Vector2(2, 14);
        this.attachmentMass = new Vector2(3, 14);
        this.draw = draw;
        force = new Vector2();
        vector = new Vector2();
        initialize();
        updatePoints();
    }

    void initialize() {
        currentLength = equilibriumDistance;
        oldLength = equilibriumDistance;

        vector.x = initialAttachment.x - attachmentMass.x;
        vector.y = initialAttachment.y - attachmentMass.y;

        for (int i = 0; i < getItemCount(); i++) {
            this.points[i] = new Vector2(attachmentMass.x + springPercentages[i][0] * width, attachmentMass.y + springPercentages[i][1] * equilibriumDistance);
            this.rotatedPoints[i] = new Vector2(springPercentages[i][1] * equilibriumDistance, springPercentages[i][0] * width);

            points[i].x = points[0].x + MathUtils.cos(rotatedPoints[i].angleRad() + vector.angleRad()) * rotatedPoints[i].len();
            points[i].y = points[0].y + MathUtils.sin(rotatedPoints[i].angleRad() + vector.angleRad()) * rotatedPoints[i].len();
        }
    }

    Vector2 updateForce(Vector2 leftAtom, Vector2 rightAtom, float timeDelta) {
        points[0].x = leftAtom.x;
        points[0].y = leftAtom.y;

        // the point where spring is connected to the moving mass
        points[springPercentages.length - 1].x = rightAtom.x;
        points[springPercentages.length - 1].y = rightAtom.y;

        // calculate the difference vector
        vector.x = rightAtom.x - points[0].x;
        vector.y = rightAtom.y - points[0].y;

        vector.setAngleRad(vector.angleRad());

        oldLength = currentLength;
        currentLength = vector.len();

        float displacement = currentLength - equilibriumDistance;
        float velocity = (currentLength - oldLength) / timeDelta;

        // calculate force vector
        float force_abs = displacement * stiffness + velocity * damping;
        float force_angle = vector.angleRad();

        force.setAngleRad(force_angle);
        force.x = force_abs * MathUtils.cos(force_angle);
        force.y = force_abs * MathUtils.sin(force_angle);

        return force;
    }

    void updatePoints() {
        for (int i = 1; i < getItemCount(); i++) {
            rotatedPoints[i].x = springPercentages[i][1] * currentLength;

            points[i].x = points[0].x + MathUtils.cos(rotatedPoints[i].angleRad() + vector.angleRad()) * rotatedPoints[i].len();
            points[i].y = points[0].y + MathUtils.sin(rotatedPoints[i].angleRad() + vector.angleRad()) * rotatedPoints[i].len();
        }
    }

    public int getItemCount() {
        return springPercentages.length;
    }

    public Vector2[] getPoints() {
        return points;
    }
}
