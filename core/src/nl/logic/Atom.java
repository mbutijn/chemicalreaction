package nl.logic;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import com.badlogic.gdx.math.*;

public class Atom extends Circle{
    private final float mass, diameter;
    private float angleContactPoint;
    private final Vector2 initialPosition, acceleration, position;
    private Vector2 velocity;
    boolean attached;
    public char element;
    boolean collided = false;
    Atom bounceOffAtom = null;

    Atom(char element, Vector2 initial, boolean attached) {
        this.element = element;
        this.attached = attached;
        this.diameter = element == 'h' ? 0.07f : element == 'o' ? 0.14f : element == 'c' ? 0.14f : element == 'n' ? 0.105f : 0;
        this.mass = element == 'h' ? 4 : element == 'c' ? 6 : element == 'n' ? 7 : element == 'o' ? 8 : 0;
        this.initialPosition = initial;

        position = new Vector2();
        acceleration = new Vector2();
        velocity = new Vector2();
        radius = 0.5f * diameter;

        initializeVectors();
    }

    void initializeVectors() {
        acceleration.setZero();
        velocity.setZero();
        this.x = initialPosition.x;
        this.y = initialPosition.y;
    }

    private float integrate(float output, float integrand, float timeDelta) {
        return output + integrand * timeDelta;
    }

    void processMovement(float timeDelta, List<Rectangle> walls, ArrayList allAtoms, Vector2... forces) {
        if (collided) {
            processMovementWithoutCollision(timeDelta, forces);
            collided = false;
        } else {
            if (checkWallCollision(walls)) {
                processWallCollision();
            }
            if (checkAtomCollision(allAtoms)) {
                processAtomCollision();
            } else {
                processMovementWithoutCollision(timeDelta, forces);
            }
        }
    }

    private boolean checkWallCollision(List<Rectangle> walls) {
        float leftPointAtom = x - radius;
        float rightPointAtom = x + radius;
        float bottomPointAtom = y - radius;
        float topPointAtom = y + radius;

        for (Rectangle wall : walls) {
            float leftWall = wall.x;
            float rightWall = wall.x + wall.width;
            float bottomWall = wall.y;
            float topWall = wall.y + wall.height;

            if (Intersector.overlaps(this, wall)) {
                float bottomDistance = topPointAtom - bottomWall;
                float topDistance = topWall - bottomPointAtom;
                float leftDistance = rightPointAtom - leftWall;
                float rightDistance = rightWall - leftPointAtom;
                float min = Collections.min(Arrays.asList(bottomDistance, topDistance, leftDistance, rightDistance));

                if (min == leftDistance) {
                    angleContactPoint = -0.5f * MathUtils.PI;
                    x = leftWall - 1.01f * radius; // right
                } else if (min == rightDistance) {
                    angleContactPoint = 0.5f * MathUtils.PI;
                    x = rightWall + 1.01f * radius; // left
                } else if (min == bottomDistance) {
                    y = bottomWall - 1.01f * radius;
                    angleContactPoint = MathUtils.PI; // up
                } else if (min == topDistance) {
                    angleContactPoint = 0f;
                    y = topWall + 1.01f * radius; // down
                }
                return true;
            }
        }
        return false;
    }

    private boolean checkAtomCollision(ArrayList<Atom> allAtoms){
        for (Atom atom : allAtoms){
            if (!this.equals(atom) && Intersector.overlaps(this, atom)) {
                calculateCornerPosition(atom.x, atom.y, atom.radius + radius);
                bounceOffAtom = atom;
                atom.calculateCornerPosition(x, y, atom.radius + radius);
                collided = true;
                return true;
            }
        }
        return false;
    }

    private void calculateCornerPosition(float xCollision, float yCollision, float distance){
        angleContactPoint = (float) Math.atan2(x - xCollision, y - yCollision);

        x = xCollision + distance * (float) Math.sin(angleContactPoint);
        y = yCollision + distance * (float) Math.cos(angleContactPoint);
    }

    private void processMovementWithoutCollision(float timeDelta, Vector2[] forces) {
        float forceX = 0, forceY = 0;
        for (Vector2 force : forces) {
            forceX += force.x;
            forceY += force.y;
        }

        acceleration.x = forceX / mass;
        acceleration.y = forceY / mass;

        // Get the velocity
        velocity.x = integrate(velocity.x, acceleration.x, timeDelta);
        velocity.y = integrate(velocity.y, acceleration.y, timeDelta);

        // Get the position
        x = integrate(x, velocity.x, timeDelta);
        y = integrate(y, velocity.y, timeDelta);
    }

    private void processWallCollision() {
        velocity.rotateRad(angleContactPoint);
        velocity.y *= -1;
        velocity.rotateRad(-angleContactPoint);
    }

    private void processAtomCollision() {
        velocity.rotateRad(angleContactPoint); // align reference frame with collision vector
        bounceOffAtom.velocity.rotateRad(angleContactPoint);

        float vel = velocity.y;
        velocity.y = bounceOffAtom.velocity.y;
        bounceOffAtom.velocity.y = vel;

        velocity.rotateRad(-angleContactPoint); // rotate back to original reference frame
        bounceOffAtom.velocity.rotateRad(-angleContactPoint);
    }

    void setVelocity(Vector2 velocity){
        this.velocity = velocity;
    }

    void setXVelocity(float velocity){
        this.velocity.x += velocity;
    }

    void setYVelocity(float velocity){
        this.velocity.y += velocity;
    }

    Vector2 getPosition() {
        position.x = this.x;
        position.y = this.y;
        return position;
    }

    public float getDiameter() {
        return diameter;
    }
}
