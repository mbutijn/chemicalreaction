package nl.logic;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import java.util.ArrayList;

public class Ammonia extends Molecule {
    Ammonia(String elements){
        super(elements);
    }

    @Override
    public void ArrangeAtoms(float startX, float startY) {
        float x, y;
        for (int i = 0; i < 4; i++) {
            if (i == 0) {
                x = startX - covalentLength;
                y = startY;
            } else if (i == 1) { // Nitrogen atom
                x = startX;
                y = startY;
            } else if (i == 2) {
                x = startX + 0.5f * covalentLength;
                y = startY + 0.5f * (float) Math.sqrt(3) * covalentLength;
            } else { // i == 3
                x = startX + 0.5f * covalentLength;
                y = startY - 0.5f * (float) Math.sqrt(3) * covalentLength;
            }

            Atom atom = new Atom(elements.charAt(i), new Vector2(x, y), true);
            atom.setVelocity(getStartVelocity());
            atoms.add(atom);
        }
    }

    @Override
    public void ArrangeChemicalBonds() {
        super.ArrangeChemicalBonds();

        for (int i = 0; i < 3; i++) {
            chemicalBonds.add(new ChemicalBond((float) Math.sqrt(3) * covalentLength, false));
            covalentForces.add(new Vector2(0, 0));
        }
    }

    @Override
    void updateCovalentForces(float timeDelta) {
        for (int i = 0; i < chemicalBonds.size(); i++) {
            ChemicalBond chemicalBond = chemicalBonds.get(i);
            Vector2 force;
            if (i == 0) { // on carbon and hydrogen (visible)
                force = chemicalBond.updateForce(atoms.get(0).getPosition(), atoms.get(1).getPosition(), timeDelta);
            } else if (i == 1 || i == 2) {
                force = chemicalBond.updateForce(atoms.get(1).getPosition(), atoms.get(i + 1).getPosition(), timeDelta);
            } else if (i == 3) { // on 2 hydrogen atoms (invisible)
                force = chemicalBond.updateForce(atoms.get(0).getPosition(), atoms.get(2).getPosition(), timeDelta);
            } else if ( i == 4) {
                force = chemicalBond.updateForce(atoms.get(0).getPosition(), atoms.get(3).getPosition(), timeDelta);
            } else { // i = 5
                force = chemicalBond.updateForce(atoms.get(2).getPosition(), atoms.get(3).getPosition(), timeDelta);
            }
            covalentForces.get(i).set(force);

            // Update the chemical bonds
            chemicalBond.updatePoints();
        }
    }

    void updateAtoms(float timeDelta, ArrayList<Rectangle> walls, ArrayList<Atom> allAtoms) {
        for (int i = 0; i < atoms.size(); i++) {
            Atom atom = atoms.get(i);
            if (i == 0) {
                atom.processMovement(timeDelta, walls, allAtoms, covalentForces.get(0).rotate(180), covalentForces.get(3).rotate(180), covalentForces.get(4).rotate(180));
            } else if (i == 1) {
                atom.processMovement(timeDelta, walls, allAtoms, covalentForces.get(0).rotate(180), covalentForces.get(1).rotate(180), covalentForces.get(2).rotate(180));
            } else if (i == 2) {
                atom.processMovement(timeDelta, walls, allAtoms, covalentForces.get(1).rotate(180), covalentForces.get(3).rotate(180), covalentForces.get(5).rotate(180));
            } else { // i == 3
                atom.processMovement(timeDelta, walls, allAtoms, covalentForces.get(2).rotate(180), covalentForces.get(4).rotate(180), covalentForces.get(5).rotate(180));
            }
        }
    }

}
