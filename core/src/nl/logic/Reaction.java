package nl.logic;

import java.util.ArrayList;

class Reaction {
    private final ArrayList<Atom> remainingAtoms = new ArrayList<>();
    String reacting1, reacting2, product1, product2, product3;

    Reaction(String reacting1, String reacting2, String product1, String product2, String product3) {
        this.reacting1 = reacting1;
        this.reacting2 = reacting2;
        this.product1 = product1;
        this.product2 = product2;
        this.product3 = product3;
    }

    void makeRemainingAtoms(Molecule first, Molecule second){
        remainingAtoms.clear();
        remainingAtoms.addAll(first.atoms);
        remainingAtoms.addAll(second.atoms);
    }

    ArrayList<Atom> getRearrangedAtomsProduct(String product) {
        ArrayList<Atom> rearrangedAtoms = new ArrayList<>();

        productElements:
        for (char element : product.toCharArray()){
            for (Atom atom : remainingAtoms){
                if (element == atom.element){
                    rearrangedAtoms.add(atom);
                    remainingAtoms.remove(atom);
                    continue productElements;
                }
            }
        }

        return rearrangedAtoms;
    }
}
