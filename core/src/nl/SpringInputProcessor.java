package nl;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;

public class SpringInputProcessor implements InputProcessor {
    private SpringInputEventListener listener;

    public SpringInputProcessor(SpringInputEventListener listener) {
        this.listener = listener;
    }

    @Override
    public boolean keyDown(int keyCode) {
        return false;
    }

    @Override
    public boolean keyUp(int keyCode) {
        if (listener != null && keyCode == Input.Keys.SPACE) {
            listener.spaceKeyDown();
        } else if (listener != null && keyCode == Input.Keys.ENTER) {
            listener.enterKeyDown();
        }
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
