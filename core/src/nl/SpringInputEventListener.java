package nl;

/**
 * Abstraction of the input events
 */
public interface SpringInputEventListener {
    void spaceKeyDown();

    void enterKeyDown();
}
