package nl.game.input;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;

import java.util.ArrayList;
import java.util.List;

public class GameBackButtonListener implements InputProcessor {
    private List<BackButtonListener> observer;

    public GameBackButtonListener() {
        this.observer = new ArrayList<>();
    }

    public void addBackButtonListener(BackButtonListener backButtonListener) {
        observer.add(backButtonListener);
    }

    @Override
    public boolean keyDown(int keycode) {
        if (keycode == Input.Keys.BACK) {
            for (BackButtonListener backButtonListener : observer) {
                backButtonListener.onBackButton();
            }
        }
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    public interface BackButtonListener {
        void onBackButton();
    }

}
