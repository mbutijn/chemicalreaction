package nl.game.input;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import nl.states.GameStateManager;

import static nl.states.GameStateType.PAUSED;

class GameInputListener implements InputProcessor {

    private GameStateManager gameStateManager;

    public GameInputListener(GameStateManager gameStateManager) {
        this.gameStateManager = gameStateManager;
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        if (keycode == Input.Keys.ESCAPE || keycode == Input.Keys.BACK) {
            gameStateManager.updateGameStateType(PAUSED);
            return true;
        }
        if (keycode == Input.Keys.UP) {
            gameStateManager.gameKeyUp(keycode);
            return true;
        }
        if (keycode == Input.Keys.DOWN) {
            gameStateManager.gameKeyUp(keycode);
            return true;
        }
        if (keycode == Input.Keys.RIGHT) {
            gameStateManager.gameKeyUp(keycode);
            return true;
        }
        if (keycode == Input.Keys.LEFT) {
            gameStateManager.gameKeyUp(keycode);
            return true;
        }
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
