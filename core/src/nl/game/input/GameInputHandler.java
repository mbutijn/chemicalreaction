package nl.game.input;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.scenes.scene2d.Stage;
import nl.GameAdapter;
import nl.states.GameStateManager;

public class GameInputHandler {

    private final GameStateManager gameStateManager;
    private GameBackButtonListener buttonListener;

    public GameInputHandler(GameStateManager gameStateManager, Stage stage, boolean game) {
        this.gameStateManager = gameStateManager;
        InputMultiplexer im;

        if (GameAdapter.applicationType == Application.ApplicationType.Android) {
            im = game ? getAndroidGameInputMultiplexer(stage) : getAndroidInputMultiplexer(stage);
        } else {
            im = game ? getDesktopGameInputMultiplexer(stage) : getDesktopInputMultiplexer(stage);
        }

        Gdx.input.setInputProcessor(im);
    }

    private InputMultiplexer getDesktopInputMultiplexer(Stage stage) {
        InputMultiplexer im = new InputMultiplexer();
        im.addProcessor(stage);
        return im;
    }

    private InputMultiplexer getDesktopGameInputMultiplexer(Stage stage) {
        InputMultiplexer im = new InputMultiplexer();
        im.addProcessor(stage);
        im.addProcessor(new GameInputListener(gameStateManager));
        return im;
    }

    private InputMultiplexer getAndroidGameInputMultiplexer(Stage stage) {
        InputMultiplexer im = new InputMultiplexer();
        GestureDetector gestureDetector = new GestureDetector(new GameGestureListener());
        buttonListener = new GameBackButtonListener();
        gestureDetector.setLongPressSeconds(0.3f);

        im.addProcessor(gestureDetector);
        im.addProcessor(buttonListener);
        im.addProcessor(stage);
        return im;
    }

    private InputMultiplexer getAndroidInputMultiplexer(Stage stage) {
        InputMultiplexer im = new InputMultiplexer();
        buttonListener = new GameBackButtonListener();
        im.addProcessor(buttonListener);
        im.addProcessor(stage);
        return im;
    }

    public GameBackButtonListener getGameBackButtonListener() {
        return buttonListener;
    }
}