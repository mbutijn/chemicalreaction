package nl.game.utils;

import com.badlogic.gdx.Gdx;

public class ResolutionManager {

    public static int getRealWidth() {
        return Gdx.graphics.getWidth();
    }

    public static int getDensityWidth() {
        return (int) (Gdx.graphics.getWidth() / Gdx.graphics.getDensity());
    }

    public static int getRealHeight() {
        return Gdx.graphics.getHeight();
    }

    public static int getDensityHeight() {
        return (int) (Gdx.graphics.getHeight() / Gdx.graphics.getDensity());
    }

    public static float density(float value) {
        return value * getDensity() / 1.2f;
    }

    public static float densityCenter(float value) {
        return value * getDensity();
    }

    private static float getDensity() {
        float size = 2f;
        if (ResolutionManager.getDensityHeight() < 480) {
            size = 1f;
        }
        return Gdx.graphics.getDensity() * size;
    }

}
