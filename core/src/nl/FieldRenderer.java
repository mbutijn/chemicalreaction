package nl;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import nl.logic.*;

import java.util.ArrayList;

/**
 * Responsible for rendering the simulation, other logic classes should not handle rendering
 */
public class FieldRenderer {
    private final GameAssetManager gameAssetManager;
    private Sprite carbonSprite, hydrogenSprite, oxygenSprite, nitrogenSprite;
    private ShapeRenderer shapeRenderer;
    private SpriteBatch spriteBatch;

    private OrthographicCamera cam;

    public FieldRenderer(GameAssetManager gameAssetManager) {
        this.gameAssetManager = gameAssetManager;
        shapeRenderer = new ShapeRenderer();
        spriteBatch = new SpriteBatch();

        float w = Gdx.graphics.getWidth();
        float h = Gdx.graphics.getHeight();

        // Constructs a new OrthographicCamera, using the given viewport width and height
        // Height is multiplied by aspect ratio.
        cam = new OrthographicCamera(10, 10 * (h / w));

        cam.position.set(cam.viewportWidth / 2f, cam.viewportHeight / 2f, 0);
        cam.update();
    }

    public void render(Simulation simulation) {
        cam.update();
        spriteBatch.setProjectionMatrix(cam.combined);
        shapeRenderer.setProjectionMatrix(cam.combined);

        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        if (carbonSprite == null) {
            carbonSprite = new Sprite(gameAssetManager.getAssetManager().get(GameAssetManager.CARBON_ATOM, Texture.class));
        }
        if (hydrogenSprite == null) {
            hydrogenSprite = new Sprite(gameAssetManager.getAssetManager().get(GameAssetManager.HYDROGEN_ATOM, Texture.class));
        }
        if (oxygenSprite == null) {
            oxygenSprite = new Sprite(gameAssetManager.getAssetManager().get(GameAssetManager.OXYGEN_ATOM, Texture.class));
        }
        if (nitrogenSprite == null) {
            nitrogenSprite = new Sprite(gameAssetManager.getAssetManager().get(GameAssetManager.NITROGEN_ATOM, Texture.class));
        }

        shapeRenderer.setAutoShapeType(true);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        for (ChemicalBond chemicalBond : simulation.getChemicalBonds()) {
            if (chemicalBond.draw) {
                for (int i = 0; i < chemicalBond.getItemCount() - 1; i++) {
                    Vector2[] points = chemicalBond.getPoints();
                    shapeRenderer.line(points[i].x, points[i].y, points[i + 1].x, points[i + 1].y);
                }
            }
        }

        shapeRenderer.end();

        if (hydrogenSprite != null && oxygenSprite != null && carbonSprite != null && nitrogenSprite != null) {
            spriteBatch.begin();
            ArrayList<Atom> atoms = simulation.getAllAtoms();
            for (Atom atom : atoms) {
                Sprite sprite = atom.element == 'h' ? hydrogenSprite : atom.element == 'o' ? oxygenSprite : atom.element == 'c' ? carbonSprite : atom.element == 'n' ? nitrogenSprite : null;
                sprite.setPosition(atom.x - atom.radius, atom.y - atom.radius);
                sprite.setSize(atom.getDiameter(), atom.getDiameter());
                sprite.draw(spriteBatch);
            }

            spriteBatch.end();
        }
    }

    public void dispose() {
        shapeRenderer.dispose();
    }
}
