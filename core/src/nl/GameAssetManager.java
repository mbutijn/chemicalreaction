package nl;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.graphics.Texture;

import com.badlogic.gdx.graphics.g2d.BitmapFont;

import static nl.game.utils.ResolutionManager.density;

public class GameAssetManager {
    private static final String LOADING_SPRITES = "Loading sprites";
    private static final String DONE_LOADING = "Done loading";
    private static final String LOADING_FONTS = "Loading fonts";

    public static final String CARBON_ATOM = "carbon.png";
    public static final String HYDROGEN_ATOM = "hydrogen.png";
    public static final String OXYGEN_ATOM = "oxygen.png";
    public static final String NITROGEN_ATOM = "nitrogen.png";

    private AssetManager assetManager;

    public GameAssetManager() {
        assetManager = new AssetManager();

        assetManager.load(CARBON_ATOM, Texture.class);
        assetManager.load(HYDROGEN_ATOM, Texture.class);
        assetManager.load(OXYGEN_ATOM, Texture.class);
        assetManager.load(NITROGEN_ATOM, Texture.class);

    }

    public AssetManager getAssetManager() {
        return assetManager;
    }

    public boolean update() {
        try {
            return assetManager.update();
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean isLoaded(String s) {
        return assetManager.isLoaded(s);
    }

    public float getProgress() {
        return assetManager.getProgress();
    }

    public <T> T get(String s, Class<T> aClass) {
        return assetManager.get(s, aClass);
    }

    public String getProgressType() {
        if (!assetManager.isLoaded(CARBON_ATOM)) {
            return LOADING_SPRITES;
        } else if (!assetManager.isLoaded(NITROGEN_ATOM)) {
            return LOADING_FONTS;
        }
        return DONE_LOADING;
    }
}
