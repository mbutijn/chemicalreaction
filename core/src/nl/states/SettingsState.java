package nl.states;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import nl.GameAdapter;
import nl.game.input.GameBackButtonListener;
import nl.game.input.GameInputHandler;
import nl.game.utils.ResolutionManager;
import nl.logic.ReactionType;
import nl.logic.Simulation;

import static nl.states.GameStateType.MENU;

public class SettingsState extends GameState implements GameBackButtonListener.BackButtonListener {

    SettingsState(GameStateManager gsm) {
        super(gsm);
    }

    @Override
    public void init() {
        super.init();
        mainTable.setWidth(ResolutionManager.getRealWidth());
        mainTable.setHeight(ResolutionManager.getRealHeight());

        initSettingsItems();
        stage.addActor(mainTable);

        GameInputHandler handler = new GameInputHandler(gameStateManager, stage, false);
        if (GameAdapter.applicationType == Application.ApplicationType.Android) {
            handler.getGameBackButtonListener().addBackButtonListener(this);
        }
    }

    private void initSettingsItems() {
        String[] settingsItems = getSettingsItems();
        TextButton.TextButtonStyle textButton = new TextButton.TextButtonStyle();
        textButton.font = boldItalicFont;
        for (String settingsItem : settingsItems) {
            mainTable.add(getSettingsItem(textButton, settingsItem)).row();
        }
    }

    private TextButton getSettingsItem(TextButton.TextButtonStyle labelStyle, final String string) {
        final TextButton textButton = new TextButton(string, labelStyle);
        textButton.setWidth(200);
        textButton.setUserObject(string);
        textButton.setColor(Color.CORAL);
        textButton.addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int buttonInt) {
                SettingsState.this.clicked(event);
                return super.touchDown(event, x, y, pointer, buttonInt);
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int buttonInt) {
                super.touchUp(event, x, y, pointer, buttonInt);
            }
        });
        textButton.setHeight(50);

        return textButton;
    }

    private String[] getSettingsItems() {
        return new String[]{"Hydrogen", "Methane", "Ammonia", "Nitrogen"};
    }

    private void clicked(InputEvent event) {
        String text = String.valueOf(event.getListenerActor().getUserObject());
        switch (text) {
            case "Hydrogen":
                Simulation.reactiontype = ReactionType.HYDROGEN;
                break;
            case "Methane":
                Simulation.reactiontype = ReactionType.METHANE;
                break;
            case "Ammonia":
                Simulation.reactiontype = ReactionType.AMMONIA;
                break;
            case "Nitrogen":
                Simulation.reactiontype = ReactionType.NITROGEN;
                break;
        }
        stopClicked();
    }

    @Override
    public void onBackButton() {
        gameStateManager.updateGameStateType(MENU);
    }

    @Override
    public void update(float dt) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void animate(float interpolate) {

    }

    private void stopClicked() {
        gameStateManager.updateGameStateType(MENU);
    }
}
