package nl.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import nl.FieldRenderer;
import nl.game.input.GameInputHandler;
import nl.logic.Simulation;

import java.util.Date;

public class PlayState extends GameState {
    private Simulation simulation;
    private FieldRenderer renderer;
    private long didPressBackAt = 0;

    public PlayState(GameStateManager gameStateManager) {
        super(gameStateManager);

        simulation = new Simulation();
        renderer = new FieldRenderer(gameStateManager.getManager());
    }

    @Override
    public void init() {
        super.init();

        new GameInputHandler(gameStateManager, stage, true);
    }

    public void startGame() {

    }

    public void pauseGame() {
        gameStateManager.updateGameStateType(GameStateType.PAUSED);
    }

    public void resumeFromPause() {
        new GameInputHandler(gameStateManager, stage, true);
    }

    @Override
    public void dispose() {
        renderer.dispose();
    }

    @Override
    public void update(float dt) {
        simulation.update(dt);
        renderer.render(simulation);
//        if (Gdx.input.getAccelerometerX() < 0) {
//            simulation.moveAtomsUp();
//        }
        if (Gdx.input.getAccelerometerY() < -2) {
            simulation.moveAtomsLeft();
        }
        if (Gdx.input.getAccelerometerY() > 2) {
            simulation.moveAtomsRight();
        }
//        if (Gdx.input.getAccelerometerX() > 4) {
//            simulation.moveAtomsDown();
//        }

        if (Gdx.input.isKeyPressed(Input.Keys.BACK)) {
            if (didPressBackAt < new Date().getTime() - 10) {
                didPressBackAt = new Date().getTime();
                pauseGame();
            }
        } else {
            didPressBackAt = 0;
        }
    }

    @Override
    public void pause() {
        simulation.pause();
    }

    @Override
    public void resume() {
        simulation.resume();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void animate(float interpolate) {

    }

    public void gameKeyUp(int keycode) {
        if (keycode == Input.Keys.RIGHT) {
            simulation.moveAtomsRight();
        } else if (keycode == Input.Keys.LEFT) {
            simulation.moveAtomsLeft();
        } else if (keycode == Input.Keys.UP) {
            simulation.moveAtomsUp();
        } else if (keycode == Input.Keys.DOWN) {
            simulation.moveAtomsDown();
        }
    }
}
