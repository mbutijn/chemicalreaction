package nl.states;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import nl.GameAdapter;
import nl.game.input.GameBackButtonListener;
import nl.game.input.GameInputHandler;
import nl.game.utils.ResolutionManager;

import static nl.states.GameStateType.*;

public class MenuState extends GameState implements GameBackButtonListener.BackButtonListener {

    private boolean paused;

    MenuState(GameStateManager gsm, boolean paused) {
        super(gsm);
        this.paused = paused;
    }

    @Override
    public void init() {
        super.init();
        mainTable.setWidth(ResolutionManager.getRealWidth());
        mainTable.setHeight(ResolutionManager.getRealHeight());

        initMenuItems();
        stage.addActor(mainTable);

        GameInputHandler handler = new GameInputHandler(gameStateManager, stage, false);
        if (GameAdapter.applicationType == Application.ApplicationType.Android) {
            handler.getGameBackButtonListener().addBackButtonListener(this);
        }
    }

    private void initMenuItems() {
        String[] menuItems = getMenuItems();
        TextButton.TextButtonStyle textButton = new TextButton.TextButtonStyle();
        textButton.font = boldItalicFont;
        for (String menuItem : menuItems) {
            mainTable.add(getMenuItem(textButton, menuItem)).row();
        }
    }

    private TextButton getMenuItem(TextButton.TextButtonStyle labelStyle, final String string) {
        final TextButton textButton = new TextButton(string, labelStyle);
        textButton.setWidth(200);
        textButton.setUserObject(string);
        textButton.setColor(Color.CORAL);
        textButton.addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int buttonInt) {
                MenuState.this.clicked(event);
                return super.touchDown(event, x, y, pointer, buttonInt);
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int buttonInt) {
                super.touchUp(event, x, y, pointer, buttonInt);
            }
        });
        textButton.setHeight(50);

        return textButton;
    }

    private String[] getMenuItems() {
        String[] menuItems;
        if (paused) {
            menuItems = new String[]{"Resume", "Restart", "Quit"};
        } else {
            menuItems = new String[]{"Simulate", "Settings", "Quit"};
        }
        return menuItems;
    }

    private void clicked(InputEvent event) {
        String text = String.valueOf(event.getListenerActor().getUserObject());
        switch (text) {
            case "Simulate":
            case "Restart":
                gameStateManager.restart();
                break;
            case "Settings":
                gameStateManager.updateGameStateType(SETTINGS);
            case "Resume":
                gameStateManager.resumeSavedPlayState();
                break;
            case "Quit":
                stopClicked();
                break;
        }
    }

    @Override
    public void update(float dt) {

    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void draw() {
        super.draw();
    }

    @Override
    public void animate(float interpolate) {

    }

    @Override
    public void dispose() {
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void onBackButton() {
        stopClicked();
    }

    private void stopClicked() {
        if (paused) {
            gameStateManager.updateGameStateType(MENU);
        } else {
            Gdx.app.exit();
        }
    }
}