package nl.states;

public enum GameStateType {
    SPLASH,
    MENU,
    SETTINGS,
    PLAY,
    PAUSED,
}
