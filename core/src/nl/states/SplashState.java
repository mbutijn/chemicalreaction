package nl.states;

import java.util.Locale;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Align;
import nl.GameAdapter;
import nl.GameAssetManager;
import nl.game.input.GameBackButtonListener;
import nl.game.input.GameInputHandler;
import nl.game.utils.ResolutionManager;

import static nl.game.utils.ResolutionManager.density;
import static nl.game.utils.ResolutionManager.getRealHeight;
import static nl.states.GameStateType.MENU;

public class SplashState extends GameState implements GameBackButtonListener.BackButtonListener {

    private final Label progress;
    private final Label progressType;
    private boolean isDoneLoading;

    public SplashState(GameStateManager gameStateManager) {
        super(gameStateManager);

        Label.LabelStyle labelStyle = new Label.LabelStyle();
labelStyle.font = new BitmapFont();
        progress = new Label("Progress: 0%", labelStyle);
        progress.setPosition(0, getRealHeight() / 2f);
        progress.setWidth(ResolutionManager.getRealWidth());
        progress.setAlignment(Align.center);

        progressType = new Label("Loading sprites", labelStyle);
        progressType.setPosition(0, getRealHeight() / 2f - 30);
        progressType.setWidth(ResolutionManager.getRealWidth());
        progressType.setAlignment(Align.center);
    }

    @Override
    public void init() {
        super.init();

        stage.addActor(progress);
        stage.addActor(progressType);

        GameInputHandler handler = new GameInputHandler(gameStateManager, stage, false);
        if (GameAdapter.applicationType == Application.ApplicationType.Android) {
            handler.getGameBackButtonListener().addBackButtonListener(this);
        }
    }

    @Override
    public void update(float dt) {
        if (gameStateManager.getManager().update() && !isDoneLoading) {
            isDoneLoading = true;
            gameStateManager.updateGameStateType(MENU);
        }
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void animate(float interpolate) {

    }

    @Override
    public void draw() {
        super.draw();
        progress.setText(String.format(Locale.US, "Progress:  %.0f %%%n", gameStateManager.getManager().getProgress() * 100));
        progressType.setText(gameStateManager.getManager().getProgressType());
    }

    @Override
    public void onBackButton() {

    }
}