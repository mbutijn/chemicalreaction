package nl.states;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Array;
import nl.GameAssetManager;

import static nl.states.GameStateType.*;

public class GameStateManager {
    private GameState currentState, oldState;
    private PlayState savedPlayState;
    private float animation = 0.01f;
    private GameAssetManager manager;

    public GameStateManager() {
        manager = new GameAssetManager();
        updateGameStateType(SPLASH);
    }

    public void updateGameStateType(GameStateType newStateType) {
        if (currentState instanceof PlayState && newStateType == PAUSED) {
            currentState.pause();
            savedPlayState = (PlayState) currentState;
        } else if (newStateType == PLAY) {
            oldState = null;
        } else {
            oldState = currentState;
        }

        switch (newStateType) {
            case SPLASH:
                currentState = new SplashState(this);
                break;
            case MENU:
                savedPlayState = null;
                currentState = new MenuState(this, false);
                break;
            case SETTINGS:
                savedPlayState = null;
                currentState = new SettingsState(this);
                break;
            case PLAY:
                currentState = new PlayState(this);
                break;
            case PAUSED:
                currentState = new MenuState(this, true);
                break;
        }
        currentState.init();
    }

    public void resumeSavedPlayState() {
        if (savedPlayState != null) {
            this.currentState = savedPlayState;
            PlayState playState = (PlayState) this.currentState;
            playState.resumeFromPause();
        }
    }

    public void restart() {
        if (savedPlayState != null) {
            savedPlayState = null;
        }
        PlayState playState = new PlayState(this);
        playState.startGame();
        currentState = playState;
        currentState.init();
    }

    public void update(float dt) {
        currentState.update(dt);
        if (oldState != null) {
            oldState.update(dt);
        }
    }

    public void draw() {
        currentState.draw();
        if (oldState != null) {
            float interpolate = Interpolation.sineOut.apply(animation);
            animation += 0.04;

            oldState.draw();
            if (oldState.stage == null) {
                return;
            }

            animateAlpha(interpolate, oldState.stage.getActors());
            animateAlpha(-interpolate, currentState.stage.getActors());
            currentState.animate(interpolate);
            if (animation > 1) {
                oldState = null;
                animation = 0.01f;
            }
        }
    }

    private void animateAlpha(float interpolate, Array<Actor> actors) {
        for (Actor actor : actors) {
            Color color = actor.getColor();
            color.a = -interpolate + (interpolate <= 0 ? 0.f : +1.f);
            actor.setColor(color);
        }
    }

    public void dispose() {
        currentState.dispose();
        if (oldState != null) {
            oldState.dispose();
        }
    }

    public void pause() {
        if (currentState instanceof PlayState) {
            ((PlayState) currentState).pauseGame();
        } else {
            currentState.pause();
            if (oldState != null) {
                oldState.pause();
            }
        }
    }

    public void resume() {
        currentState.resume();
        if (oldState != null) {
            oldState.resume();
        }
    }

    public void resize(int width, int height) {
        currentState.resize(width, height);
        if (oldState != null) {
            oldState.resize(width, height);
        }
    }

    GameAssetManager getManager() {
        return manager;
    }

    public void gameKeyUp(int keycode) {
        if (currentState instanceof PlayState) {
            ((PlayState) currentState).gameKeyUp(keycode);
        }
    }
}
