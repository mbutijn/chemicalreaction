package nl.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;

import static nl.GameAssetManager.*;

public abstract class GameState {
    protected GameStateManager gameStateManager;
    protected Stage stage;
    protected SpriteBatch spriteBatch;
    protected OrthographicCamera camera;

    protected BitmapFont font;
    protected BitmapFont boldItalicFont;
    protected Table mainTable;
    protected TextButton.TextButtonStyle style;

    protected GameState(GameStateManager gameStateManager) {
        this.gameStateManager = gameStateManager;
    }

    public void init() {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        spriteBatch = new SpriteBatch();
        camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

        stage = new Stage();
        Gdx.input.setInputProcessor(stage);

        if (gameStateManager.getManager().isLoaded(NITROGEN_ATOM)) {
            font = new BitmapFont();
            font.getRegion().getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
            font.getData().setScale(4f,4f);

            boldItalicFont = new BitmapFont();
            boldItalicFont.getRegion().getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
            boldItalicFont.getData().setScale(4f,4f);

            style = new TextButton.TextButtonStyle();
            style.font = font;
            mainTable = new Table();
        }
    }

    public abstract void update(float dt);
    public abstract void pause();
    public abstract void resume();
    public abstract void dispose();
    public abstract void resize(int width, int height);
    public abstract void animate(float interpolate);

    public void draw() {
        if (spriteBatch != null) {
            spriteBatch.setProjectionMatrix(camera.combined);
            spriteBatch.begin();

            stage.act();

            if (!(this instanceof PlayState)) {
                Gdx.gl.glClearColor(0, 0, 0, 1);
                Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
            }
            stage.draw();

            spriteBatch.end();
        }
    }

}
