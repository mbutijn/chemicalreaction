package nl;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class DesktopLauncher {

	public static void main(String[] arg) {
		DesktopLauncher desktopLauncher = new DesktopLauncher();
		desktopLauncher.startProgram(1024, 768, 2);
	}

	private void startProgram(int width, int height, int samples) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = width;
		config.height = height;
		config.samples = samples;
		config.resizable = false;
		config.title = "Chemical Reactor";
		new LwjglApplication(new GameAdapter(1, "Desktop"), config);
	}
}
